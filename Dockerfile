#
FROM ubuntu:18.10
WORKDIR /home/app
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
RUN apt-get install -y apt-utils
ENV TZ=America/Denver
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ >/etc/timezone
RUN apt-get install -y tzdata
RUN echo "=== Done installing Ubuntu."
#
RUN apt-get install -y python3
RUN apt-cache policy python3
RUN apt-get install -y python3-pip
RUN python3 -m pip install pandas
RUN python3 -m pip install numpy
RUN python3 -m pip install scipy
RUN python3 -m pip install matplotlib
RUN python3 -m pip install scikit-learn
RUN echo "=== Done installing Python."
#
RUN apt-get install -y apache2
RUN apt-cache policy apache2
RUN apt-get install -y php
RUN a2enmod cgi proxy proxy_http
RUN echo "=== Done installing Apache."
#
COPY ./conf/apache2.conf /etc/apache2/apache2.conf
COPY ./conf/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY ./conf/serve-cgi-bin.conf /etc/apache2/conf-available/serve-cgi-bin.conf
RUN echo "=== Apache config copied."
#
COPY ./htdocs/ /home/www/htdocs/
COPY ./cgi-bin/ /home/www/cgi-bin/
RUN mkdir /home/www/cgi-bin/logs
RUN chmod 777 /home/www/cgi-bin/logs
RUN mkdir /home/www/htdocs/scratch
RUN chmod 777 /home/www/htdocs/scratch
RUN echo "=== Web content copied."
#
CMD ["apachectl", "-D", "FOREGROUND"]
#
