# `DOCKER_CGIS`

Docker container with CGI webapps, mostly Python.

Based on [Docker Hub `ubuntu`](https://hub.docker.com/_/ubuntu).

## Dependencies

* Ubuntu 18.10
* Python 3.6+
* Scikit-learn, Matplotlib, Pandas, Scipy, Numpy

## Building and Running

See [`Go_DockerBuild.sh`](sh/Go_DockerBuild.sh)
and [`Go_DockerRun.sh`](sh/Go_DockerRun.sh)

```
sudo docker build -t "cgis"
sudo docker run -dit --name "cgis_container" -p 9090:80 "cgis"
```

## Usage

See <http://localhost:9090/cgi-bin/skils.cgi>, (SKILS =
"Scikit-learn interactive learning system").

The demo should produce an app like:

[demo_app_screenshot.png](doc/demo_app_screenshot.png)

and a plot like:

[demo_plot_screenshot.png](doc/demo_plot_screenshot.png)
