#!/bin/sh
###
set -e
#
sudo systemctl -l status docker
#
sudo docker version
sudo docker info
#
INAME="cgis"
CNAME="${INAME}_container"
#
###
# Monitor and examine.
sudo docker images
sudo docker ps -a
sudo docker container ls -a
sudo docker exec ${CNAME} cat /etc/apache2/apache2.conf
sudo docker exec ${CNAME} cat /etc/apache2/conf-available/serve-cgi-bin.conf
sudo docker exec ${CNAME} ls -lR /home/app/apache2
sudo docker exec ${CNAME} cat /var/log/apache2/error.log
sudo docker logs --details --tail 100 ${CNAME}
#
