#!/bin/sh
###
set -e
#
INAME="cgis"
CNAME="${INAME}_container"
#
###
# Instantiate and run container.
# -dit = --detached --interactive --tty
sudo docker run -dit --name ${CNAME} -p 9090:80 ${INAME}
chromium http://localhost:9090/
#
