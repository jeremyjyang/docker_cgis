#!/usr/bin/env python3
#############################################################################
### textsimilarity.cgi - 
#############################################################################
import os,sys,cgi,time,re

import htm_utils

#############################################################################
def JavaScript():
  return '''\
function checkform(form)
{
  if (!form.intxt.value && !form.infile.value) {
    alert('ERROR: No input specified');
    return 0;
  }
  return 1;
}
function go_txtsim(form)
{
  if (!checkform(form)) return;
  form.txtsim.value='TRUE';
  form.submit();
}
function go_init(form)
{
  form.intxt.value='';
  form.verbose.checked=false;
}
'''

#############################################################################
def PrintForm():
  print('<FORM NAME="mainform" ACTION="%s/%s.out" METHOD="POST" ENCTYPE="multipart/form-data">'%(CGIURL,PROG))
  print('<INPUT TYPE=HIDDEN NAME="txtsim">')
  print('<INPUT TYPE=HIDDEN NAME="download">')
  print('<TABLE WIDTH="100%%"><TR><TD WIDTH="20%%"><H2>%s</H2></TD>'%PROG)
  print('<TD>- text similarity</TD>')
  print('<TD ALIGN=RIGHT>')
  print('<BUTTON TYPE=BUTTON onClick="void window.open(\'%s?help=TRUE\',\'helpwin\',\'width=600,height=400,scrollbars=1,resizable=1\')"><B>Help</B></BUTTON>'%CGIURL)
  print('&nbsp;<BUTTON TYPE=BUTTON onClick="window.location.replace(\'%s\')"><B>Reset</B></BUTTON>'%CGIURL)
  print('</TD></TR></TABLE>')
  print('<HR>')
  print('<TABLE WIDTH=100% CELLPADDING=5 CELLSPACING=5>')
  print('<TR BGCOLOR="#CCCCCC">')
  print('<TD WIDTH="60%" VALIGN=TOP>')
  print('<B>input:</B>&nbsp;')
  print('upload:<INPUT TYPE="FILE" NAME="infile">&nbsp;')
  print('file2txt:<INPUT TYPE=CHECKBOX NAME="file2txt" VALUE="CHECKED" %s><BR>'%FILE2TXT)
  print('or paste...<BR>')
  print('<TEXTAREA NAME="intxt" ROWS=12 COLS=60 WRAP=OFF>%s</TEXTAREA>'%INTXT)
  print('</TD>')
  print('<TD VALIGN=TOP>')
  print('<INPUT TYPE=CHECKBOX NAME="verbose" VALUE="CHECKED" %s>verbose<BR>'%VERBOSE)
  print('</TD></TR>')
  print('<TR>')
  print('<TD COLSPAN=3 ALIGN=CENTER>')
  print('<BUTTON TYPE=BUTTON onClick="go_txtsim(this.form)"><B>&nbsp; go &nbsp;</B></BUTTON>')
  print('</TD></TR>')
  print('</TABLE>')
  print('</FORM>')

#############################################################################
def Initialize():
  global FORM,VERBOSE,INTXT,FILE2TXT,LOG,CGIURL,PROG,DATE
  global ERRORS,OUTPUTS
  ERRORS=[]; OUTPUTS=[];
  FORM=cgi.FieldStorage(keep_blank_values=1)
  CGIURL=os.environ['SCRIPT_NAME']
  PROG=os.path.basename(sys.argv[0])

  ERRORS.append('<IMG SRC="/images/biocomp_logo_only.gif">%s web app from UNM Translational Informatics Division.'%PROG)

  DATE=time.strftime('%Y%m%d%H%M',time.localtime())

  VERBOSE=FORM.getvalue('verbose')
  INTXT=FORM.getvalue('intxt')
  FILE2TXT=FORM.getvalue('file2txt')

  logfile='./logs/'+PROG+'.log'
  logfields=['ip']
  if os.access(logfile,os.R_OK):
    tmp=open(logfile)
    lines=tmp.readlines()
    tmp.close()
    if len(lines)>1:
      date=lines[1].split('\t')[0][:8]
      date='%s/%s/%s'%(date[4:6],date[6:8],date[:4])
    ERRORS.append('%s has been used %d times since %s'%(PROG,len(lines)-1,date))

  if os.access(logfile,os.W_OK):
    LOG=open(logfile,'a')
    if os.path.getsize(logfile)==0:
      LOG.write('date\t' + '\t'.join(logfields) + '\n')
  else:
    LOG=open(logfile,'w+')
    LOG.write('date\t' + '\t'.join(logfields) + '\n')
  if not LOG:
    ERRORS.append('LOGFILE error.')
    return False

  if not FORM.getvalue('txtsim'): return True

  ### STUFF FOR A RUN

  if FORM.has_key('infile') and FORM['infile'].file and FORM['infile'].filename:
    fdata=FORM['infile'].value
    IMS.openstring(fdata)
    if FORM.getvalue('file2txt'):
      INTXT=fdata
  elif FORM.has_key('intxt') and FORM['intxt'].value:
    INTXT=FORM['intxt'].value
  else:
    ERRORS.append('ERROR: No input data.')
    return False

  return True

#############################################################################
def Help():
  print('''\
<HR>
<H3>%(PROG)s help</H3>
Not yet functional.
<P>
'''%{'PROG':PROG})

#############################################################################
if __name__=='__main__':
  ok=Initialize()
  if not ok:
    htm_utils.PrintHeader(PROG,JavaScript(),None)
    htm_utils.PrintFooter(ERRORS)
  elif FORM.getvalue('run_txtsim'):
    htm_utils.PrintHeader(PROG,JavaScript(),None)
    PrintForm()
    htm_utils.PrintOutput(OUTPUTS)
    htm_utils.PrintFooter(ERRORS)
    LOG.write('%s\t%s\n'%(DATE,os.environ['REMOTE_ADDR']))
  elif FORM.getvalue('help'):
    htm_utils.PrintHeader(PROG,JavaScript(),None)
    Help()
    htm_utils.PrintFooter(ERRORS)
  else:
    htm_utils.PrintHeader(PROG,JavaScript(),None)
    PrintForm()
    print('<SCRIPT>go_init(window.document.mainform)</SCRIPT>')
    htm_utils.PrintFooter(ERRORS)
