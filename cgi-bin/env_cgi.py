import os
#
#############################################################################
HTML_SUBDIR=''	## Needed if subdir used.  Specify with leading '/'.
#scratchdir:
if os.uname()[0]=="Darwin":
  scratchdir='/Library/WebServer/Documents'+HTML_SUBDIR+'/scratch'
else:
  scratchdir='/home/www/htdocs'+HTML_SUBDIR+'/scratch'
scratchdirurl=HTML_SUBDIR+'/scratch'
#
# 1 day = 86400 sec
# 1 hour = 3600 sec
scratchdir_lifetime=3600
#
#############################################################################
#OE:
os.environ['OE_DIR']='/home/app/openeye'
os.environ['OE_LICENSE']=os.environ['OE_DIR']+'/etc/oe_license.txt'
#os.environ['OE_ARCH']='suse-SLE11-g++4.3-x64'
os.environ['OE_ARCH']='suse-SLE11-x64'
#
#############################################################################
#depictions:
#DEPICT_TOOL='rdkit'
DEPICT_TOOL='openeye'
#DEPICT_TOOL='dingo'
#############################################################################
