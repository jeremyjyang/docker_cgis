#! /usr/bin/env python3
"""
	showimg.cgi - very simple, just output imgfile specified 
	A previous version of this cgi was used to hack into a server via reverse
	shell and remote file inclusion.  The insecure os.system('cat %s'%imgfile)
	is exploitable as the "imgfile" can be specified by the attacker and could
	result in execution of a script.
"""
import os,sys,cgi

form=cgi.FieldStorage(keep_blank_values=1)

imgfile=form.getvalue('imgfile')
imgfmt=form.getvalue('imgfmt', 'png')

sys.stdout.buffer.write(('Content-type: image/%s\n\n'%imgfmt).encode('utf-8'))

if not os.access(imgfile, os.R_OK):
  sys.exit(1)

f=open(imgfile, "rb")
while True:
  byte=f.read(1)
  if not byte: break
  sys.stdout.buffer.write(byte)

sys.stdout.flush()
f.close()
