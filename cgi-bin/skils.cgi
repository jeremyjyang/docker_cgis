#!/usr/bin/env python3
"""
	skils.cgi - Scikit-learn Interactive Learning System

	Probably this functionality better served by a Jupyter notebook!
"""
#############################################################################
import os,sys,cgi,re,time,tempfile,csv
import urllib,urllib.request
import random

import sklearn.metrics

### local stuff
import purgescratchdirs
import htm_utils
import env_cgi

import sklearn_utils

#############################################################################
def JavaScript():
  response = urllib.request.urlopen('http://localhost/data/ckd_clean.csv')
  js=('var DEMO_DATASET_CKD=`'+response.read().decode('utf-8')+'`;\n')
  js+='''\
function checkform(form)
{
  if (!form.intxt.value && !form.infile.value) {
    alert('ERROR: No input file specified');
    return 0;
  }
  return 1;
}
function go_skils(form, runmode)
{
  if (!checkform(form)) return;
  form.runmode.value=runmode;
  form.submit();
}
function go_demo(form)
{
  go_init(form);
  form.title.value='Chronic Kidney Disease (UCI ML Archive)';
  form.intxt.value=DEMO_DATASET_CKD;
  go_skils(form, 'skils')
}
function go_init(form)
{
  form.file2txt.checked=true;
  form.verbose.checked=false;
  form.intxt.value='';
  form.title.value='';
  form.svm_cparam.value='1.0';
  form.svm_gamma.value='auto';
  form.split_pct.value='20';
  //form.eptag.value='';
  var i;
  for (i=0;i<form.delim.length;++i)      //radio
    if (form.delim[i].value=='comma')
      form.delim[i].checked=true;
  for (i=0;i<form.svm_kern.length;++i)      //radio
    if (form.svm_kern[i].value=='linear')
      form.svm_kern[i].checked=true;
}
'''
  return js

#############################################################################
def CheckInputCSV(infile):
  title = TITLE if TITLE else FILENAME if FILENAME else ""
  delim=DELIMS[DELIM]
  #csvdialect = sklearn_utils.CsvDialect("skl")
  OUTPUTS.append('<H2>Dataset Check:</H2>')

  try:
    fin = open(infile)
    X, y, ftags, etag = sklearn_utils.ReadDataset(fin, verbose=VERBOSE)
  except Exception as e:
    OUTPUTS.append('ERROR: (ReadDataset)  %s'%(e))
    return False

  if X.shape[0]>0 and X.shape[1]>0:
    OUTPUTS.append('<BLOCKQUOTE>Dataset OK.')
    OUTPUTS.append('Dataset N_cases = %d and N_features = %d'%(X.shape[0], X.shape[1]))
    OUTPUTS.append('Dataset feature tags: %s'%(str(ftags)))
    OUTPUTS.append('Dataset endpoint tag: "%s"</BLOCKQUOTE>'%(etag))
    ok = True
  elif X.shape[0]==0:
    OUTPUTS.append('<BLOCKQUOTE>Dataset NOT OK, with N_cases = 0</BLOCKQUOTE>')
    ok = False
  elif X.shape[1]==0:
    OUTPUTS.append('<BLOCKQUOTE>Dataset NOT OK, with N_features = 0</BLOCKQUOTE>')
    ok = False

  return ok

#############################################################################
def SplitInputCSV(infile):
  try:
    split_pct = int(SPLIT_PCT)
  except:
    split_pct = 20

  fin = open(infile)
  fout = tempfile.NamedTemporaryFile(mode='w', prefix=PREFIX, suffix='_data_train.txt', delete=False)
  fout_split = tempfile.NamedTemporaryFile(mode='w', prefix=PREFIX, suffix='_data_test.txt', delete=False)
  n_in, n_train, n_test = sklearn_utils.SplitCSV(fin, fout, fout_split, split_pct=split_pct)
  n_out=n_train+n_test
  fout.close()
  fout_split.close()

  OUTPUTS.append('<H2>Dataset Split:</H2>')
  OUTPUTS.append('<BLOCKQUOTE>split_pct: %d'%(split_pct))
  OUTPUTS.append('Dataset cases read: %d'%(n_in))
  OUTPUTS.append('Dataset cases out (TRAIN): %d (%.1f%%)'%(n_train, 100.0*n_train/n_out))
  OUTPUTS.append('Dataset cases out (TEST): %d (%.1f%%)'%(n_test, 100.0*n_test/n_out))
  OUTPUTS.append('Dataset cases out (TOTAL): %d</BLOCKQUOTE>'%(n_out))

  fname='%s_data_train.csv'%(APPNAME)
  bhtm1=('<FORM ACTION="%s/%s" METHOD="post">'%(CGIURL, fname))
  bhtm1+=('<INPUT TYPE="hidden" NAME="downloadfile" VALUE="%s">'%fout.name)
  bhtm1+=('<BUTTON TYPE="button" onClick="this.form.submit()">')
  bhtm1+=('%s (%s)</BUTTON><BR>(%d cases)</FORM>'%(fname, htm_utils.NiceBytes(os.stat(fout.name).st_size), n_train))

  fname='%s_data_test.csv'%(APPNAME)
  bhtm2=('<FORM ACTION="%s/%s" METHOD="post">'%(CGIURL, fname))
  bhtm2+=('<INPUT TYPE="hidden" NAME="downloadfile" VALUE="%s">'%fout_split.name)
  bhtm2+=('<BUTTON TYPE="button" onClick="this.form.submit()">')
  bhtm2+=('%s (%s)</BUTTON><BR>(%d cases)</FORM>'%(fname, htm_utils.NiceBytes(os.stat(fout_split.name).st_size), n_test))
  OUTPUTS.append('<BLOCKQUOTE><b>Download:</b><TABLE><TR><TD>%s</TD><TD>%s</TD></TR></TABLE></BLOCKQUOTE>'%(bhtm1, bhtm2))

  return fout.name, fout_split.name

#############################################################################
def PrintForm():
  DELIM_COMMA=''; DELIM_TAB=''; DELIM_SPACE='';
  if DELIM=='tab': DELIM_TAB='CHECKED';
  elif DELIM=='space': DELIM_SPACE='CHECKED';
  else: DELIM_COMMA='CHECKED';

  KERNEL_LINEAR=''; KERNEL_RBF=''; KERNEL_POLY=''; KERNEL_SIGMOID='';
  if SVM_KERNEL_TYPE=='linear': KERNEL_LINEAR='CHECKED'
  elif SVM_KERNEL_TYPE=='sigmoid': KERNEL_SIGMOID='CHECKED'
  elif SVM_KERNEL_TYPE=='poly': KERNEL_POLY='CHECKED'
  else: KERNEL_RBF='CHECKED'

  print('<FORM NAME="mainform" ACTION="%s" METHOD="post" ENCTYPE="multipart/form-data">'%(CGIURL))
  print('<INPUT TYPE="hidden" NAME="runmode">')
  print('<TABLE WIDTH="100%%"><TR><TD WIDTH="20%%"><H2>%s</H2></TD>'%APPNAME)
  print('<TD>- Scikit-learn interactive learning system</TD>')
  print('<TD ALIGN="right">')
  print('<BUTTON TYPE="button" onClick="void window.open(\'%s?help=TRUE\',\'helpwin\',\'width=600,height=400,scrollbars=1,resizable=1\')"><B>Help</B></BUTTON>'%CGIURL)
  print('<BUTTON TYPE="button" onClick="go_demo(this.form)"><B>Demo</B></BUTTON>')
  print('<BUTTON TYPE="button" onClick="window.location.replace(\'%s\')"><B>Reset</B></BUTTON>'%CGIURL)
  print('</TD></TR></TABLE>')
  print('<HR>')
  print('<TABLE BGCOLOR="#AACCEE" WIDTH=100% CELLPADDING=3>')
  print('<TR>')
  print('<TD VALIGN="top">')
  print('<TABLE WIDTH="100%" CELLSPACING=5 CELLPADDING=2>')
  print('<TR><TD>')
  print('csv file upload:<INPUT TYPE="file" NAME="infile">&nbsp;')
  print('file2txt:<INPUT TYPE="checkbox" NAME="file2txt" VALUE="CHECKED" %s><BR>'%FILE2TXT)
  print('or paste...<BR>')
  print('<TEXTAREA NAME="intxt" ROWS="12" COLS="70" WRAP="off">%s</TEXTAREA>'%INTXT)
  print('</TD></TR>')
  print('</TABLE>')
  print('</TD>')
  print('<TD VALIGN="top">')
  print('</TD>')
  print('<TD VALIGN="top">')
  print('<TABLE WIDTH="100%" CELLSPACING=0 CELLPADDING=2>')
  print('<TR><TD COLSPAN="2" VALIGN="top"><B>Input:</B></TD></TR>')
  print('<TR><TD ALIGN="right" VALIGN="top">')
  print('field delimiter:</TD>')
  print('<TD><INPUT TYPE="radio" NAME="delim" VALUE="comma" %s>comma'%DELIM_COMMA)
  print('<INPUT TYPE="radio" NAME="delim" VALUE="tab" %s>tab'%DELIM_TAB)
  print('<INPUT TYPE="radio" NAME="delim" VALUE="space" %s>space'%DELIM_SPACE)
  print('</TD></TR>')
  print('<TR><TD ALIGN="right" VALIGN="top">split_pct:</TD><TD><INPUT TYPE="text" SIZE="8" NAME="split_pct" VALUE="%s"></TD></TR>'%SPLIT_PCT)
  print('<TR><TD COLSPAN="2" VALIGN="top"><HR><B>SVM:</B></TD></TR>')
  print('<TR><TD ALIGN="right" VALIGN="top">')
  print('SVM kernel type:</TD><TD>')
  print('<INPUT TYPE="radio" NAME="svm_kern" VALUE="linear" %s>Linear'%KERNEL_LINEAR)
  print('<INPUT TYPE="radio" NAME="svm_kern" VALUE="poly" %s>Polynomial'%KERNEL_POLY)
  print('<INPUT TYPE="radio" NAME="svm_kern" VALUE="sigmoid" %s>Sigmoid'%KERNEL_SIGMOID)
  print('<INPUT TYPE="radio" NAME="svm_kern" VALUE="rbf" %s>Radius Basis Function'%KERNEL_RBF)
  print('</TD></TR>')
  print('<TR><TD ALIGN="right" VALIGN="top">Cparam:</TD><TD><INPUT TYPE="text" SIZE="8" NAME="svm_cparam" VALUE="%s"></TD></TR>'%SVM_CPARAM)
  print('<TR><TD ALIGN="right" VALIGN="top">gamma:</TD><TD><INPUT TYPE="text" SIZE="8" NAME="svm_gamma" VALUE="%s"></TD></TR>'%SVM_GAMMA)
  print('<TR><TD COLSPAN="2" VALIGN="top"><HR><B>Plot:</B></TD></TR>')
  print('<TR><TD COLSPAN="2" VALIGN="top"><HR><B>Output:</B></TD></TR>')
  print('<TR><TD ALIGN="right" VALIGN="top">Project Title:</TD><TD><INPUT TYPE="text" SIZE=24 NAME="title" VALUE="%s"></TD></TR>'%TITLE)
  print('<TR><TD COLSPAN="2" VALIGN="top"><HR><B>Misc:</B></TD></TR>')
  print('<TR><TD ALIGN="right" VALIGN="top">verbose:</TD><TD><INPUT TYPE="checkbox" NAME="verbose" VALUE="CHECKED" %s></TD></TR>'%VERBOSE)
  print('</TABLE>')
  print('</TD>')
  print('</TR>')
  print('<TR>')
  print('<TD ALIGN="left"><BUTTON TYPE="button" onClick="go_skils(this.form,\'check_data\')"><B>Check data</B></BUTTON>')
  print('<BUTTON TYPE=BUTTON onClick="go_skils(this.form,\'split_data\')"><B>Split data</B></BUTTON></TD>')
  print('<TD ALIGN=CENTER><BUTTON TYPE="button" onClick="go_skils(this.form,\'skils\')"><B>Go '+APPNAME+'</B></BUTTON></TD>')
  print('<TD></TD></TR>')
  print('</TABLE>')
  print('</FORM>')

#############################################################################
def SkilsClassifier():
  try:
    svm_gamma=float(SVM_GAMMA)
  except:
    svm_gamma='auto'
  clf = sklearn_utils.SVMClassifierFactory(SVM_KERNEL_TYPE, float(SVM_CPARAM), gamma=svm_gamma, verbose=False)
  if not clf:
    ERRORS.append('ERROR: Failed to instantiate SVM.')
    return None
  OUTPUTS.append('<H2>Classifier Parameters:</H2>')
  OUTPUTS.append('<BLOCKQUOTE>CLASSIFIER_TYPE: %s'%(re.sub(r"^.*'(.*)'.*$", r'\1', str(type(clf)))))
  OUTPUTS.append(ClassifierParamTableHtm(clf))
  OUTPUTS.append('</BLOCKQUOTE>')
  return clf

#############################################################################
def ClassifierParamTableHtm(clf):
  params = clf.get_params()
  htm='<TABLE BORDER="0" CELLSPACING="2">\n'
  htm+='<TR><TD COLSPAN="2" ALIGN="center"><B>Classifier Parameters</B></TD></TR>\n'
  for key in sorted(params.keys()):
    htm+=('<TD ALIGN="right">%s:</TD><TD BGCOLOR="white">%s</TD></TR>\n'%(key, params[key]))
  htm+='</TABLE>\n'
  return htm

#############################################################################
def SkilsTrain(clf, X_train, y_train):
  ERRORS.append('X_train.shape: %s ; y_train.shape: %s'%(str(X_train.shape), str(y_train.shape)))
  clf.fit(X_train, y_train)

#############################################################################
def SkilsTest(clf, X_test, y_test, eptag):
  OUTPUTS.append('<H2>Classifier Test Results:</H2>')

  mean_acc = clf.score(X_test, y_test)
  y_pred = clf.predict(X_test)
  prec = sklearn.metrics.precision_score(y_test, y_pred)
  rec = sklearn.metrics.recall_score(y_test, y_pred)
  mcc = sklearn.metrics.matthews_corrcoef(y_test, y_pred)
  f1 = sklearn.metrics.f1_score(y_test, y_pred)
  cmat = sklearn.metrics.confusion_matrix(y_test, y_pred)
  OUTPUTS.append('<BLOCKQUOTE>'+ConfusionMatrixHtm(cmat, eptag))
  OUTPUTS.append('N_cases: %6d ; N_features: %6d ; N_classes: %6d'%(len(X_test), X_test.shape[1], len(set(y_test))))
  OUTPUTS.append('mean_accuracy = %.4f ; precision = %.4f ; recall = %.4f'%(mean_acc, prec, rec))
  OUTPUTS.append('F1_score = %.4f ; Matthew\'s Correlation Coefficient = %.4f</BLOCKQUOTE>'%(f1, mcc))

  fout_test = tempfile.NamedTemporaryFile(mode='w', prefix=PREFIX, suffix='_data_test_out.txt', delete=False)
  fout_test.write((','.join(['f%d'%j for j in range(1, X_test.shape[1]+1)]))+',label\n')
  for i in range(len(X_test)):
    fout_test.write((','.join(map(lambda f:'%.3f'%f, X_test[i])))+(',%d\n'%y_pred[i]))
  fout_test.close()

  clf_type = (re.sub(r"^.*'(.*)'.*$", r'\1', str(type(clf))))
  fname='%s_%s_data_test_out.csv'%(APPNAME, clf_type)
  bhtm=('<FORM ACTION="%s/%s" METHOD="post">'%(CGIURL, fname))
  bhtm+=('<INPUT TYPE="hidden" NAME="downloadfile" VALUE="%s">'%fout_test.name)
  bhtm+=('<BUTTON TYPE="button" onClick="this.form.submit()">')
  bhtm+=('%s (%s)</BUTTON><BR>(%d cases)</FORM>'%(fname, htm_utils.NiceBytes(os.stat(fout_test.name).st_size), len(X_test)))
  OUTPUTS.append('<BLOCKQUOTE><b>Download:</b> %s</BLOCKQUOTE>'%bhtm)

#############################################################################
def ConfusionMatrixHtm(cmat, eptag):
  tn, fp, fn, tp = cmat.ravel()
  htm='<TABLE BORDER="0" CELLSPACING="5" CELLPADDING="5">\n'
  htm+='<TR><TD COLSPAN="2" ROWSPAN="2"><SMALL><I>CONFUSION MATRIX</I></SMALL></TD>\n'
  htm+=('<TD COLSPAN="2" ALIGN="center" BGCOLOR="#CCCCCC">Actual %s</TD></TR>\n'%eptag)
  htm+='<TR><TD ALIGN="center" BGCOLOR="#CCCCCC">True</TD><TD ALIGN="center" BGCOLOR="#CCCCCC">False</TD></TR>\n'
  htm+=('<TR><TD ROWSPAN="2" BGCOLOR="#CCCCCC">Predicted %s</TD>\n'%eptag)
  htm+=('<TD VALIGN="middle" BGCOLOR="#CCCCCC">True</TD><TD ALIGN="center" BGCOLOR="white">TP = %d</TD><TD ALIGN="center" BGCOLOR="white">FP = %d</TD></TR>\n'%(tp, fp))
  htm+=('<TR><TD VALIGN="middle" BGCOLOR="#CCCCCC">False</TD><TD ALIGN="center" BGCOLOR="white">FN = %d</TD><TD ALIGN="center" BGCOLOR="white">TN = %d</TD></TR>\n'%(fn, tn))
  htm+='</TABLE>\n'
  return htm

#############################################################################
def SkilsPlotPCA(clf, X_train, y_train, X_test, y_test, cnames, eptag, title, subtitle):
  '''Can we do a zoom plot button and window with resize control?  Must have data cached.'''
  width, height, dpi = 7, 5, 100
  title = title if title else FILENAME if FILENAME else APPNAME+'_test'
  fout_plot = tempfile.NamedTemporaryFile(mode='w+b', prefix=PREFIX, suffix='_plot.png', delete=True)
  fout_plot.close()
  sklearn_utils.PlotPCA(clf, X_train, y_train, X_test, y_test, cnames, eptag, title, subtitle, width, height, dpi, fout_plot.name, verbose=0)
  OUTPUTS.append('<H2>Classifier PCA Plot:</H2>')
  #ERRORS.append('DEBUG: plot file: %s'%fout_plot.name)
  imghtm=ImgfileHtm(SHOWIMGURL, fout_plot.name, width*dpi, height*dpi)
  OUTPUTS.append("<BLOCKQUOTE><TABLE BORDER><TR><TD>"+imghtm+"</TD></TR></TABLE></BLOCKQUOTE>")

#############################################################################
def ImgfileHtm(appurl, fplot, w, h):
  opts=('imgfile=%s&imgfmt=png'%(urllib.parse.quote(fplot)))
  imgurl=('%s?%s'%(appurl, opts))
  htm=('<IMG WIDTH="%d" HEIGHT="%d" SRC="%s">'%(w, h, imgurl))
  htm='<A HREF="javascript:void(0)" onClick="javascript:go_zoom_img(\'%s\',\'%s\',%d,%d,\'imgwin\')">%s</A>'%(appurl, opts, w, h, htm)
  return htm

#############################################################################
def Initialize():
  global FORM, VERBOSE, INTXT, FILE2TXT, LOG, CGIURL, PROG, APPNAME, DATE
  global ERRORS, OUTPUTS, NMAX
  ERRORS=[]; OUTPUTS=[];
  FORM=cgi.FieldStorage(keep_blank_values=1)
  CGIURL=os.environ['SCRIPT_NAME']
  PROG=os.path.basename(sys.argv[0])
  APPNAME='SKILS'

  logohtm="<TABLE CELLSPACING=5 CELLPADDING=5>\n<TR><TD>"
  href="http://medicine.unm.edu/informatics/"
  imghtm=('<IMG SRC="/images/biocomp_logo_only.gif">')
  tiphtm=('%s web app from UNM Translational Informatics Divsion'%APPNAME)
  logohtm+=(htm_utils.HtmTipper(imghtm, '', tiphtm, href, 200, "white"))

  href="http://scikit-learn.org"
  imghtm=('<IMG HEIGHT="60" SRC="/images/scikit-learn-logo-small.png">')
  tiphtm='Scikit-learn.org'
  logohtm+=(htm_utils.HtmTipper(imghtm, '', tiphtm, href, 200, "white"))
  logohtm+="</TD></TR></TABLE>"
  ERRORS.append(logohtm)

  URLHOST=os.environ['SERVER_NAME']
  VERBOSE=FORM.getvalue('verbose', '')
  INTXT=FORM.getvalue('intxt')
  FILE2TXT=FORM.getvalue('file2txt', '')
  NMAX=1000
  DATE=time.strftime('%Y%m%d%H%M', time.localtime())

  global SHOWIMGURL, TITLE, SPLIT_PCT
  urldir=os.path.dirname(os.environ['REQUEST_URI'])
  SHOWIMGURL=urldir+'/showimg.cgi'

  TITLE=FORM.getvalue('title')
  SPLIT_PCT=FORM.getvalue('split_pct')

  global DELIM, DELIMS, ACCURACY, PRECISION
  DELIM=FORM.getvalue('delim')
  DELIMS={'tab':'\t', 'space':' ', 'comma':','}
  ACCURACY=FORM.getvalue('accuracy')

  global SVM_KERNEL_TYPE, SVM_CPARAM, SVM_GAMMA
  SVM_KERNEL_TYPE=FORM.getvalue('svm_kern')
  SVM_CPARAM=FORM.getvalue('svm_cparam')
  SVM_GAMMA=FORM.getvalue('svm_gamma')

  logfile='./logs/'+PROG+'.log'
  logfields=['ip', 'Kernel', 'Ndata']
  if os.access(logfile, os.R_OK):
    tmp=open(logfile)
    lines=tmp.readlines()
    tmp.close()
    if len(lines)>1:
      date=lines[1].split('\t')[0][:8]
      date='%s/%s/%s'%(date[4:6], date[6:8], date[:4])
      ERRORS.append('%s has been used %d times since %s.' %(PROG, len(lines)-1, date))

  if os.access(logfile, os.W_OK):
    LOG=open(logfile, 'a')
    if os.path.getsize(logfile)==0:
      LOG.write('date\t' + '\t'.join(logfields) + '\n')
  else:
    LOG=open(logfile, 'w+')
    LOG.write('date\t' + '\t'.join(logfields) + '\n')
  if not LOG:
    ERRORS.append('LOGFILE error.')
    return False


  ### Assure that writeable tmp directory exists.
  if not os.access(env_cgi.scratchdir, os.F_OK):
    ERRORS.append('ERROR: missing scratch dir "%s"'%env_cgi.scratchdir)
    return False
  elif not os.access(env_cgi.scratchdir, os.W_OK):
    ERRORS.append('ERROR: non-writable scratch dir "%s"'%env_cgi.scratchdir)
    return False

  tempfile.tempdir=env_cgi.scratchdir
  global DELFILES
  DELFILES=[]

  if not FORM.getvalue('runmode'):
    return True

  ### STUFF FOR A RUN #######################################################

  global TS, PREFIX
  TS=time.strftime('%Y%m%d%H%M%S', time.localtime())
  PREFIX=re.sub('\..*$', '', PROG)+'.'+TS+'.'+str(random.randint(100, 999))

  global INDATA, INFILE, FILENAME
  FILENAME=""
  if 'infile' in FORM and FORM['infile'].file and FORM['infile'].filename:
    f=tempfile.NamedTemporaryFile(mode='w+b', prefix=PREFIX, suffix='_data.txt', delete=False)
    f.write(FORM['infile'].value)
    INDATA=FORM['infile'].value.decode('utf8')
    INFILE=f.name
    f.close()
    DELFILES.append(INFILE)
    if FILE2TXT:
      INTXT=''
      i_line=0;
      for line in INDATA.splitlines():
        INTXT+=(line+'\n')
        i_line+=1
        if i_line==NMAX:
          ERRORS.append('WARNING: Input dataset truncated at NMAX = %d.'%NMAX)
          break
    FILENAME=FORM['infile'].filename
  elif 'intxt' in FORM and FORM['intxt'].value:
    INDATA=FORM['intxt'].value
    f=tempfile.NamedTemporaryFile(mode='w+b', prefix=PREFIX, suffix='_data.txt', delete=False)
    f.write(bytes(INDATA, 'utf8'))
    f.close()
    INFILE=f.name
    DELFILES.append(INFILE)
    INTXT=INDATA
  else:
    ERRORS.append('ERROR: No input data.')
    return False

  n_lines = len(INDATA.splitlines())
  #ERRORS.append('DEBUG: f.name = "%s" ; lines = %d'%(f.name, n_lines))
  LOG.write('%s\t%s\t%s\t%d\n'%(DATE, os.environ['REMOTE_ADDR'], SVM_KERNEL_TYPE, n_lines-1))

  return True

#############################################################################
def Help():
  print('''\
<HR>
<H2>%(APPNAME)s help</H2>
<P>
<B>SKILS = Scikit-learn Interactive Learning System</B>
<P>
This web app provides convenient access to selected Scikit-learn machine learning (ML) 
classification methods on <U>ML-ready datasets</U>.
This tool is not suitable for large datasets, but analyzing a sample of a large 
dataset could be useful by informing next steps.  This tool may also be educational,
and may assist in first-pass descriptive and exploratory data analysis.
<P>
<H3>ML-ready datasets</H3>
Defined here as an input file meeting the following requirements:
<OL>
<LI>CSV with header.  Or delimiter may be tab or space.
<LI>All values must be numeric.  (Scikit-learn can vectorize nominal features, so
this pre-processing functionality may be added later.)
<LI>Header tags must be quoted (Scikit-learn is fussy).
<LI>No missing values.
<LI>The last column is the endpoint, a.k.a. class label.  This specifies what
the classification model will be trained to predict from the features in the
preceding columns.
</OL>
<P>
<H3>Glossary:</H3>
<BLOCKQUOTE>
True Postive Rate = TP / P<BR>
False Positive Rate = FP / N<BR>
precision = TP / (TP+FP)<BR>
accuracy = (TP+TN) / (P+N)<BR>
<BR>
<I>where:<BR>
TP = true positives<BR>
P = total positives<BR>
FP = false positives<BR>
TN = true negatives<BR>
FN = false negatives<BR>
N = total negatives<BR>
</I>
</BLOCKQUOTE>
<P>
<B>Built with:</B>
<UL>
<LI><A HREF="http://scikit-learn.org">Scikit-learn.org</A>
<LI><A HREF="http://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html">API: sklearn.svm.SVC</A>
<LI><A HREF="http://www.numpy.org">NumPy</A>
<LI><A HREF="http://matplotlib.org">Matplotlib</A>
<LI><A HREF="http://www.python.org">Python3</A>
</UL>
<P>
NMAX = %(NMAX)s<BR>
<I>(maximum number of cases)</I><BR>
<P>
author: Jeremy Yang
<P>
'''%{'APPNAME':APPNAME,'NMAX':NMAX})

#############################################################################
if __name__=='__main__':
  ok=Initialize()
  if not ok:
    htm_utils.PrintHeader(PROG, JavaScript())
    htm_utils.PrintFooter(ERRORS)
  elif FORM.getvalue('runmode')=='check_data':
    htm_utils.PrintHeader(PROG, JavaScript())
    PrintForm()
    ok=CheckInputCSV(INFILE)
    if ok:
      htm_utils.PrintOutput(OUTPUTS)
    else:
      ERRORS.append("ERROR: cannot read dataset.")
    htm_utils.PrintFooter(ERRORS)
  elif FORM.getvalue('runmode')=='split_data':
    htm_utils.PrintHeader(PROG, JavaScript())
    PrintForm()
    ofile_train, ofile_test = SplitInputCSV(INFILE)
    htm_utils.PrintOutput(OUTPUTS)
    htm_utils.PrintFooter(ERRORS)
  elif FORM.getvalue('runmode')=='skils':
    htm_utils.PrintHeader(PROG, JavaScript())
    PrintForm()
    ofile_train, ofile_test = SplitInputCSV(INFILE)
    #
    clf = SkilsClassifier()
    fin_train = open(ofile_train)
    X_train, y_train, ftags, eptag = sklearn_utils.ReadDataset(fin_train)
    SkilsTrain(clf, X_train, y_train)
    #
    fin_test = open(ofile_test)
    X_test, y_test, ftags, eptag = sklearn_utils.ReadDataset(fin_test)
    SkilsTest(clf, X_test, y_test, eptag)
    #
    clf_type = (re.sub(r"^.*'(.*)'.*$", r'\1', str(type(clf))))
    SkilsPlotPCA(clf, X_train, y_train, X_test, y_test, None, eptag, TITLE, clf_type)
    #
    htm_utils.PrintOutput(OUTPUTS)
    htm_utils.PrintFooter(ERRORS)
  elif FORM.getvalue('downloadfile'):
    htm_utils.DownloadFile(FORM.getvalue('downloadfile'))
  elif FORM.getvalue('help'):
    htm_utils.PrintHeader(PROG, '')
    Help()
    htm_utils.PrintFooter(ERRORS)
  else:
    htm_utils.PrintHeader(PROG, JavaScript())
    PrintForm()
    print('<SCRIPT>go_init(window.document.mainform)</SCRIPT>')
    htm_utils.PrintFooter(ERRORS)

  htm_utils.Cleanup(DELFILES)
  purgescratchdirs.PurgeScratchDirs([env_cgi.scratchdir], env_cgi.scratchdir_lifetime, 0)
